<?php
	require('../db/dbcon.php');
	session_start();
	if(!isset($_SESSION['user_id']) || !isset($_SESSION['user_name']))
	{
		echo "<script type='text/javascript'>window.location= '../login.html';</script>";
	}
	else
	{
		$user_id = $_SESSION['user_id'];
		if(!isset($_SESSION['exam_id']))
		{
			echo "<script type='text/javascript'>window.location= 'stud_home.php';</script>";
		}
		else
		{
			$exam_id = $_SESSION['exam_id'];
			
			$query="Select * from exams where id='$exam_id' and tea_id='$user_id'";
			$check_if_exist= mysqli_query($con,$query);
			
			$check_result=mysqli_num_rows($check_if_exist);
			if($check_result>0)
				{
				$row=mysqli_fetch_assoc($check_if_exist);
				$exam_name=$row['name'];
				$exam_date= $row['exam_date'];
				$no_questions= $row['questions_no'];
				}
		}
	}
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>theshit</title>
    <meta name="" content="">
    <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
    <link rel="manifest" href="manifest.json">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i">
    <link rel="stylesheet" href="../assets/fonts/fontawesome-all.min.css">
</head>

<body class="flex-sm-shrink-1 flex-md-shrink-1 flex-lg-shrink-1">
    <div id="content" style="margin-bottom: 0px;height: 900px;max-height: 900px;max-width: 1920px;">
        <nav class="navbar navbar-light navbar-expand sticky-top bg-white text-center shadow flex-fill mb-4 topbar static-top" style="margin-top: 0px;padding-left: 11px;padding-right: 11px;">
            <div class="container-fluid"><a class="navbar-brand" href="#" style="margin-right: 16px;font-size: 44px;">Exampod</a>
                <div class="d-sm-flex d-xl-flex flex-shrink-1 flex-fill flex-sm-shrink-1 flex-md-shrink-1 justify-content-xl-center" style="width: 278px;height: 69px;padding-top: 11px;margin-right: 0px;margin-left: 5px;">
                    <h1 style="font-size: 19px;width: 156px;padding-top: 0px;height: 35px;margin-top: 11px;margin-bottom: 11px;">Questions Saved:</h1>
                    <h1 class="text-left" style="margin-right: 6px;width: 177px;margin-bottom: 8px;margin-top: -1px;"><?php echo $no_questions; ?></h1>
                </div>
                <div class="d-md-flex justify-content-md-end" style="width: 265px;max-height: 60px;height: 63px;padding-top: 12px;padding-bottom: 10px;margin-right: 0px;margin-left: 0px;padding-right: 0px;"><button class="btn btn-danger" type="button" style="margin-right: 4px;">Cancel</button><button class="btn btn-success" type="button" style="margin-left: 4px;background-color: rgb(28,148,200); margin-right: 4px;">Save</button><button class="btn btn-success"
                        type="button" style="margin-left: 4px;">Save &amp; Exit</button></div>
            </div>
        </nav>
        <div class="container-fluid">
            <h3 class="text-dark mb-1"><?php echo $exam_name; ?></h3>
        </div>
        <div class="container-fluid flex-grow-1 flex-shrink-1 flex-fill" style="height: 883px;padding-top: 24px;margin-bottom: 0px;">
            <div class="flex-grow-1 flex-shrink-1 flex-fill" style="margin: 0;margin-top: 16px;height: 927px;max-height: 2000;">
                <?php
					for($i=1;$i<=$no_questions; $i++)
						{
							$str="Q".$i;
							$Astr="ans".$i;
							$Ostr="O".$i;
							$A="A".$i;
							$B="B".$i;
							$C="C".$i;
							$D="D".$i;
							
							$sql_query = "Select * from mcqexam where id='$exam_id' and no='$i'";
							$sql_result = mysqli_query($con,$sql_query);
							$check_query=mysqli_num_rows($sql_result);
							if($check_query>0)
								{
								$q_row=mysqli_fetch_assoc($sql_result);
								$question=$q_row['Ques'];
								$ansA= $q_row['A'];
								$ansB= $q_row['B'];
								$ansC= $q_row['C'];
								$ansD= $q_row['D'];
								$Correct= $q_row['Correct'];
							
								
								
								echo "
									<div class='card' style='margin-top: 5px;margin-left: 0px;margin-bottom: 5px;'>
										<div class='card-body shadow'>
											<h6 class='text-muted card-subtitle mb-2'>$str.&nbsp;</h6>
											<div class='row'>
												<div class='col'><textarea style='width: 1108px;max-width: 100%;min-width: 100%;' spellcheck='true' minlength='2'>$question</textarea></div>
											</div>
											<div class='row'>
												<div class='col'>
													<div class='form-check'>
														<div><input type='radio' name='$Astr' value='A' id='A'>   A. <input type='text' name='$A' value='$ansA'></input><br></div>
														<div style='margin-top: 2px'><input type='radio' name='$Astr' value='B' id='B'>   B. <input type='text' name='$B' value='$ansB'></input><br></div>
														<div style='margin-top: 2px'><input type='radio' name='$Astr' value='C' id='C'>   C. <input type='text' name='$C' value='$ansC'></input><br></div>
														<div style='margin-top: 2px'><input type='radio' name='$Astr' value='D' id='D'>   D. <input type='text' name='$D' value='$ansD'></input><br></div>
														<script>
															document.getElementById('B').checked = true;
														</script>
													</div>
												</div>
											</div>
										</div>
									</div>
								";
								
								if($Correct=="A")
								{
									echo "<script>
										document.getElementById('A').checked = true;
									</script>";
								}
								else if($Correct=="B")
								{
									echo "<script>
										document.getElementById('B').checked = true;
									</script>";
								}
								else if($Correct=="C")
								{
									echo "<script>
										document.getElementById('C').checked = true;
									</script>";
								}
								else if($Correct=="D")
								{
									echo "<script>
										document.getElementById('D').checked = true;
									</script>";
								}
							}
							else
							{
								echo "
									<div class='card' style='margin-top: 5px;margin-left: 0px;margin-bottom: 5px;'>
										<div class='card-body shadow'>
											<h6 class='text-muted card-subtitle mb-2'>$str.&nbsp;</h6>
											<div class='row'>
												<div class='col'><textarea style='width: 1108px;max-width: 100%;min-width: 100%;' spellcheck='true' minlength='2'></textarea></div>
											</div>
											<div class='row'>
												<div class='col'>
													<div class='form-check'>
														<div><input type='radio' name='$Astr' value='A' required>   A. <input type='text' name='$A' required></input><br></div>
														<div style='margin-top: 2px'><input type='radio' name='$Astr' value='B' required>   B. <input type='text' name='$B' required></input><br></div>
														<div style='margin-top: 2px'><input type='radio' name='$Astr' value='C' required>   C. <input type='text' name='$C' required></input><br></div>
														<div style='margin-top: 2px'><input type='radio' name='$Astr' value='D' required>   D. <input type='text' name='$D' required></input><br></div>
													</div>
												</div>
											</div>
										</div>
									</div>
								";
							}
						}
				?>
                
            </div>
        </div>
    </div>
    <div class="modal fade" role="dialog" tabindex="-1" id="submitmodal">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header" style="padding-bottom: 16px;padding-left: 220px;padding-right: 16px;">
                    <h4 class="modal-title">Submit</h4><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button></div>
                <div class="modal-body" style="height: 58px;">
                    <p class="text-center">Are you sure you want to submit now?</p>
                </div>
                <div class="modal-footer"><button class="btn btn-light" type="button" data-dismiss="modal" style="background-color: rgb(231,74,59);">Cancel</button><button class="btn btn-primary" type="button" style="background-color: rgb(28,200,138);">Submit</button></div>
            </div>
        </div>
    </div>
    <div class="modal fade" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header" style="padding-bottom: 16px;padding-left: 237px;padding-right: 16px;">
                    <h4 class="modal-title" style="padding-left: 0px;">Exit</h4><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button></div>
                <div class="modal-body" style="height: 76px;">
                    <p class="text-center">Are you sure you want to forfeit the exam? You will not be able to redo this exam!</p>
                </div>
                <div class="modal-footer"><button class="btn btn-light" type="button" data-dismiss="modal" style="background-color: rgb(231,74,59);">Cancel</button><button class="btn btn-primary" type="button" style="background-color: rgb(28,200,138);">Yes, I want to forfeit</button></div>
            </div>
        </div>
    </div>
    <script src="../assets/js/jquery.min.js"></script>
    <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="../assets/js/chart.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.js"></script>
    <script src="../assets/js/script.min.js"></script>
</body>

</html>