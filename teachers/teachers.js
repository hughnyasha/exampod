$("button#proceed").click( function() {
 
	if( $("#exam_name").val() == "" || $("#exam_date").val() == "" || $("#start_time").val() == "" || $("#end_time").val() == "" || $("#duration").val() == "" || $("#no_questions").val() == "" || $("#password").val() == "" || $("#repeat_password").val() == "" || $("#instructions").val() == "")
		$("div#ack").html("Please enter all details!");
	else
		$.post( $("#examdetsform").attr("action"),
		$("#examdetsform :input").serializeArray(),function(data) {
		$("div#ack").html(data);
		});
 
	$("#examdetsform").submit( function() {
	   return false;
	});
 
});