<?php
	require('dbcon.php');
	
?>

<!DOCTYPE HTML>
<!--
	Hyperspace by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	
<h1 align="center">Create Exam</h1>
<form method="post" action="create_exam.php">
<table align="center">
<tr>
	<th align="left">Exam name         </th>
	<td><input type="text" name="exname" required></td>
</tr>
<tr>
	<th align="left">No. of questions     </th>
	<td><input type="number" name="exno" required></td>
</tr>
<tr>
	<th align="left">Exam Pin     </th>
	<td><input type="text" name="expin" required pattern=".{4,}" maxlength="4"></td>
</tr>
<tr>
	<th align="left">Confirm Exam Pin     </th>
	<td><input type="text" name="con_expin" required pattern=".{4,}" maxlength="4"></td>
</tr>
<tr>
	<td colspan="2" align="center">
	<input type="submit" value="     Create    ">
	</td>
</tr>

<!--<tr>
	<td><br><p><a   href="registration.php">Create Account</a></p></td>
	<td><br><p align="right"><a   href="passreset.php">Forgot Password</a></p></td>
</tr>-->
</table>
</form>

</html>