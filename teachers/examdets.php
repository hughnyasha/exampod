<?php
	require('../db/dbcon.php');
	session_start();
	if(!isset($_SESSION['user_id']) || !isset($_SESSION['user_name']))
	{
		echo "<script type='text/javascript'>window.location= '../login.html';</script>";
	}
	else
	{
		$user_id = $_SESSION['user_id'];
		$user_name = $_SESSION['user_name'];
		
		$exam_name = $_POST['exam_name'];
		$exam_date = $_POST['exam_date'];
		$start_time = $_POST['start_time'];
		$end_time = $_POST['end_time'];
		$duration = $_POST['duration'];
		$no_questions = $_POST['no_questions'];
		$instructions = $_POST['instructions'];
		$password = $_POST['password'];
		$repeat_password = $_POST['repeat_password'];
		
		$hashed_pass = hash('adler32',$password);
		
		if($password!=$repeat_password)
		{
			echo "<script type='text/javascript'>alert('Passwords do not match!');</script>";
		}
		else
		{
			$sql = "INSERT INTO `exams`(`tea_id`, `name`, `exam_date`, `start_time`, `end_time`, `duration`, `questions_no`, `exam_pin`, `instructions`) VALUES ('$user_id','$exam_name','$exam_date','$start_time','$end_time','$duration','$no_questions','$hashed_pass', '$instructions')";
			$result= mysqli_query($con,$sql);
			if(!$result)
				{
					echo "<script type='text/javascript'>alert('Failed to save exam details!');</script>";
				}
			else
				{
					$query="Select * from exams where tea_id='$user_id' and exam_pin='$hashed_pass' and name='$exam_name' and exam_date='$exam_date' and questions_no='$no_questions'";
					$check_if_exist= mysqli_query($con,$query);
					
					$check_result=mysqli_num_rows($check_if_exist);
					if($check_result>0)
					{
						$row=mysqli_fetch_assoc($check_if_exist);
						$exam_id=$row['id'];
						$_SESSION['exam_id'] = $exam_id;
						echo "<script type='text/javascript'>alert('Exam details saved.');</script>";
						echo "<script type='text/javascript'>window.location= 'create_exam.php';</script>";
					}
					else
					{
						echo "<script type='text/javascript'>alert('Exam details not saved!');</script>";
					}
				}
		}
	}
?>