	const start = 0.005;
	let time = start * 60 * 60;
	
	
	const countdownEl = document.getElementById('countdown');
	
	setInterval(updateCountdown, 1000);
	
	function updateCountdown(){
		if(time>=0)
		{
		const hours = Math.floor(time / 60 / 60);
		let left = time - (hours * 60 * 60);
		const minutes = Math.floor(left / 60);
		let secleft = left % 60;
		let seconds = secleft;
		
		mins = minutes < 10 ? '0' + minutes : minutes;
		seconds = seconds < 10 ? '0' + seconds : seconds;
		
		countdownEl.innerHTML = `${hours}:${mins}:${seconds}`;
		//countdownEl.innerHTML = `${hours}:${minutes}:${seconds}`;
		time--;
		
		if(hours==0 && mins==0 && seconds==0)
		{
			stop();
		}
		}
	}
	
	function stop()
	{
		countdownEl.innerHTML = `0:00:00`;
		alert("Time is up!");
		submitOnTimeEnd();
	}
	
	function submitOnTimeEnd()
	{
		location.replace('test/exam');
		window.location= 'stud_home.php';
	}
	
	function submit()
	{
		var r = confirm("Are you sure you want to submit now?");
		updateCountdown();
		if (r == true) {
			location.replace('test/exam');
			window.location= 'stud_home.php';
		} 
		else
		{
		
		}
	}
	
	