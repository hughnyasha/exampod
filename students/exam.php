<?php
	require('../db/dbcon.php');
	session_start();
	
	$exam_id = $_SESSION['exam_id'];
	$exam_password = $_SESSION['exam_password'];
	
	$q_arr = array();
	echo "<form action='ansside.php' method='POST'>";
	$i=1;
	$sql = "Select*from exams where id='$exam_id' and exam_pin='$exam_password'";
	$result=mysqli_query($con,$sql);
	$count=mysqli_num_rows($result);
	if($count==0)
	{
		echo "<script type='text/javascript'>alert('incorrect exam details');</script>";
		header("refresh:0;url=enterExamdets.php");
	}
	else
	{
	$exam_sql = "Select*from mcqexam where id='$exam_id'";
	$result=mysqli_query($con,$exam_sql);
	}
	
?>

<!DOCTYPE html>
<html onload="startExam()">

<head onload="startExam()">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>exs</title>
    <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i">
    <link rel="stylesheet" href="../assets/fonts/fontawesome-all.min.css">
</head>

<body id="body" class="flex-sm-shrink-1 flex-md-shrink-1 flex-lg-shrink-1" onload="countdownTimeStart(), startExam()" style="user-select: none;">
    <div id="content" style="margin-bottom: 0px;height: 900px;max-height: 900px;max-width: 1920px;">
        <nav class="navbar navbar-light navbar-expand sticky-top bg-white text-center shadow flex-fill mb-4 topbar static-top" style="margin-top: 0px;padding-left: 11px;padding-right: 11px;">
            <div class="container-fluid"><a class="navbar-brand" href="#" style="margin-right: 16px;font-size: 44px;">Exampod</a>
                <div class="d-sm-flex d-xl-flex flex-shrink-1 flex-fill flex-sm-shrink-1 flex-md-shrink-1 justify-content-xl-center" style="width: 342px;height: 69px;padding-top: 11px;margin-right: 0px;margin-left: 5px;">
                    <h1 style="font-size: 19px;width: 151px;padding-top: 0px;height: 28px;margin-top: 11px;margin-bottom: 11px;">Time Remaining:</h1>
                    <div id=""><h1 id="countdown" style="margin-right: 6px;width: 177px;"></h1></div>
                </div>
                <div class="d-md-flex justify-content-md-end" style="width: 165px;max-height: 60px;height: 63px;padding-top: 12px;padding-bottom: 10px;margin-right: 0px;margin-left: 0px;padding-right: 0px;"><button class="btn btn-danger" type="button" style="margin-right: 4px;">Cancel</button><button class="btn btn-success" type="submit" style="margin-left: 4px;" onclick="submit()">Submit</button></div>
            </div>
        </nav>
        <div class="container-fluid">
            <h3 class="text-dark mb-1">Exam name</h3>
        </div>
        <div class="container-fluid flex-grow-1 flex-shrink-1 flex-fill" style="height: 883px;padding-top: 24px;margin-bottom: 0px;">
            <div class="flex-grow-1 flex-shrink-1 flex-fill" style="margin: 0;margin-top: 16px;height: 927px;max-height: 2000;">
                <?php
					$sql = "Select*from mcqexam where id='$exam_id'";
					$result=mysqli_query($con,$sql);
					
					$count=mysqli_num_rows($result);
					if(($count)>0)
					{
						$_SESSION['num']=$count;
						while($row=mysqli_fetch_assoc($result))
						{
						
						$str = "Q".$i;
							echo "<div class='card' style='margin-top: 5px;margin-left: 0px;margin-bottom: 5px;'>
								<div class='card-body shadow'>
									<h6 class='text-muted card-subtitle mb-2'>Q$i.&nbsp;</h6>
									<p class='card-text'>".$row['Ques']."</p>
									<input type='radio' name='$str' value='A' required>"."A. ".$row['A']."</input>
									<br>
									<input type='radio' name='$str' value='B' required>"."B. ".$row['B']."</input>
									<br>
									<input type='radio' name='$str' value='C' required>"."C. ".$row['C']."</input>
									<br>
									<input type='radio' name='$str' value='D' required>"."D. ".$row['D']."</input>
									<br>
								</div>
							</div>";
						$i++;
						}
					}
				?>
            </div>
        </div>
    </div>
	
	<script>
	  if (performance.navigation.type == 1) {
		alert( "Reloading is not allowed! You have been kicked out of this exam." );
		window.location = 'stud_home.php'
	  }
	
	if (document.fullscreenEnabled) {
	document.documentElement.requestFullscreen();
	}
	
	document.body.addEventListener("keydown", function event(ev) {
  
    // function to check the detection
    ev = ev || window.event;  // Event object 'ev'
    var key = ev.which || ev.keyCode; // Detecting keyCode
      
    // Detecting Ctrl
    var ctrl = ev.ctrlKey ? ev.ctrlKey : ((key === 17)
        ? true : false);
  
    // If key pressed is V and if ctrl is true.
    if (key == 86 && ctrl) {
        // print in console.
        alert("Ctrl+V is pressed.");
    }
    else if (key == 67 && ctrl) {
  
        // If key pressed is C and if ctrl is true.
        // print in console.
        alert("Ctrl+C is pressed.");
    }
	
	// If key pressed is A and if ctrl is true.
    if (key == 65 && ctrl) {
        // print in console.
        alert("Ctrl+A is pressed.");
	}
}, false);

	//Prevent closing
		window.onbeforeunload = function leave() {
		//alert("Are you sure you want to leave this page. You will not be allowed to retake the exam after leaving this page.");
	   return "Do you really want to leave our brilliant application?";
	   //if we return nothing here (just calling return;) then there will be no pop-up question at all
	   //return;
	};
	
	document.addEventListener("visibilitychange", event => {
	  if (document.visibilityState == "visible") {
		//alert("tab is active");
		document.documentElement.requestFullscreen();
	  } else {
		document.documentElement.requestFullscreen();
		alert("Switching tabs during the exam is a serious violation!");
	  }
	})
	document.documentElement.requestFullscreen();
	
	document.addEventListener('fullscreenchange', (event) => {
  // document.fullscreenElement will point to the element that
  // is in fullscreen mode if there is one. If there isn't one,
  // the value of the property is null.
  if (document.fullscreenElement) {
    console.log(`Element: ${document.fullscreenElement.id} entered full-screen mode.`);
  } else {
    alert('Do not leave full-screen mode during exam!');
	document.documentElement.requestFullscreen();
  }
});
	const start = 0.10;
	let time = start * 60 * 60;
	
	
	
	const countdownEl = document.getElementById('countdown');
	
	setInterval(updateCountdown, 1000);
	setInterval(startExam, 1000);

	
	function updateCountdown(){
		if(time>=0)
		{
		const hours = Math.floor(time / 60 / 60);
		let left = time - (hours * 60 * 60);
		const minutes = Math.floor(left / 60);
		let secleft = left % 60;
		let seconds = secleft;
		
		mins = minutes < 10 ? '0' + minutes : minutes;
		seconds = seconds < 10 ? '0' + seconds : seconds;
		
		countdownEl.innerHTML = `${hours}:${mins}:${seconds}`;
		//countdownEl.innerHTML = `${hours}:${minutes}:${seconds}`;
		time--;
		
		if(hours==0 && mins==0 && seconds==0)
		{
			stop();
		}
		}
	}
	
	function stop()
	{
		countdownEl.innerHTML = `0:00:00`;
		alert("Time is up!");
		submitOnTimeEnd();
	}
	
	function submitOnTimeEnd()
	{
		window.location.replace('test/exam');
		window.location= 'stud_home.php';
	}
	
	function submit()
	{
		var r = confirm("Are you sure you want to submit now?");
		updateCountdown();
		if (r == true) {
			window.location.replace('test/exam');
			window.location= 'stud_home.php';
		} 
		else
		{
		
		}
	}
	
	function startExam()
	{
	document.documentElement.requestFullscreen();
	}
	
</script>
	
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!-- Good alternative is to include minified file jQuery.countdownTimer.min.js -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/screenfull.js/5.1.0/screenfull.min.js"></script>
	<script src="../assets/js/jquery.min.js"></script>
    <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="../assets/js/chart.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.js"></script>
    <script src="../assets/js/script.min.js"></script>
</body>

</html>