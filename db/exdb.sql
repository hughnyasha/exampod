-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 29, 2020 at 07:12 PM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `id14619304_exdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `answers`
--

CREATE TABLE `answers` (
  `user_id` int(10) NOT NULL,
  `exam_id` int(100) NOT NULL,
  `ques_id` int(20) NOT NULL,
  `ans` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `answers`
--

INSERT INTO `answers` (`user_id`, `exam_id`, `ques_id`, `ans`) VALUES
(8, 9, 1, 'B'),
(8, 9, 2, 'C'),
(8, 9, 3, 'A');

-- --------------------------------------------------------

--
-- Table structure for table `exams`
--

CREATE TABLE `exams` (
  `id` int(11) NOT NULL,
  `tea_id` int(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `questions_no` int(3) NOT NULL,
  `exam_pin` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `exams`
--

INSERT INTO `exams` (`id`, `tea_id`, `name`, `questions_no`, `exam_pin`) VALUES
(9, 4, 'PHP', 3, 1234);

-- --------------------------------------------------------

--
-- Table structure for table `mcqexam`
--

CREATE TABLE `mcqexam` (
  `id` int(10) NOT NULL,
  `no` int(5) NOT NULL,
  `Ques` varchar(200) NOT NULL,
  `A` varchar(200) NOT NULL,
  `B` varchar(200) NOT NULL,
  `C` varchar(200) NOT NULL,
  `D` varchar(200) NOT NULL,
  `Correct` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mcqexam`
--

INSERT INTO `mcqexam` (`id`, `no`, `Ques`, `A`, `B`, `C`, `D`, `Correct`) VALUES
(9, 1, 'ANs B', 'feaw', 'grtg', 'yjhth', 'nyuun', 'B'),
(9, 2, 'ANs C', 'uyik', 'lu', 'okmuynol', 'hrdthrt', 'C'),
(9, 3, 'ANs A', 'trdgh', 'rthdrt', 'jytfh', 'hrtdrtd', 'A');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` int(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `dob` date NOT NULL,
  `email` varchar(300) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `name`, `dob`, `email`, `password`) VALUES
(1, 'Nyasha Mudoti', '2020-07-07', 'hughnyasha@gmail.com', '043901ad'),
(6, 'hugh mudoti', '2020-06-30', 'hugh@gmail.com', '00620062'),
(7, 'midea', '2020-06-30', 'm@gmail.com', '013700d5'),
(8, 'Benq', '2020-07-01', 'benq@benq.com', '040801a7'),
(9, 'wendy', '2020-08-04', 'wendy@g.com', '00620062'),
(10, 'wad', '2020-07-28', 'ad@g.com', '00620062'),
(11, 'Appy Fizz', '2020-08-04', 'appy@fizz.com', '043101bb'),
(12, 'awd', '2020-07-30', 'wda@fe', '00620062'),
(13, 'dwa', '2020-08-21', 'as@as.com', '00620062'),
(14, 'rtu', '2020-08-12', 'dads@asf', '013700d5');

-- --------------------------------------------------------

--
-- Table structure for table `teachers`
--

CREATE TABLE `teachers` (
  `id` int(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `dob` date NOT NULL,
  `email` varchar(300) NOT NULL,
  `phone` bigint(15) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `teachers`
--

INSERT INTO `teachers` (`id`, `name`, `dob`, `email`, `phone`, `password`) VALUES
(1, 'hugh mudoti', '2020-06-30', 'hughnyasha@gmail.com', 2147483647, '043901ad'),
(2, 'midea', '2020-07-01', 'm@gmail.com', 9016673223, '00620062'),
(3, '', '0000-00-00', '', 0, '00000001'),
(4, 'unknown', '2020-07-28', 'benq@benq.com', 9016673235, '00620062');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `exams`
--
ALTER TABLE `exams`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teachers`
--
ALTER TABLE `teachers`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `exams`
--
ALTER TABLE `exams`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `teachers`
--
ALTER TABLE `teachers`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
