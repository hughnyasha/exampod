<?php
	require('../db/dbcon.php');
	//input from registration
	$first_name = $_POST['first_name'];
	$last_name = $_POST['last_name'];
	$dob = $_POST['dob'];
	$email = $_POST['email'];
	$acc_type = $_POST['acc_type'];
	$password = $_POST['password'];
	$password_repeat = $_POST['password_repeat'];
	$hashed_pass = hash('adler32',$password);
	
	if($password!=$password_repeat)
	{
		echo "<script type='text/javascript'>alert('Passwords do not match!');</script>";
	}
	else
	{
		//check if account already exists as a student account
		$query="Select * from students where email='$email'";
			$check_if_exist= mysqli_query($con,$query);
			$check_result=mysqli_num_rows($check_if_exist);
			
			if($check_result>0)
				{
					echo "<script type='text/javascript'>alert('This email address is registered to an account!');</script>";
				}
			else
				{
					//chech if user account already exists as a teacher account
					$query="Select * from teachers where email='$email'";
					$check_if_exist= mysqli_query($con,$query);
					$check_result=mysqli_num_rows($check_if_exist);
					if($check_result>0)
					{
						echo "<script type='text/javascript'>alert('This email address is registered to an account!');</script>";
					}
					else
					{
						if($acc_type==1)		//Create a student account
						{  
							$sql = "INSERT INTO `students` (`first_name`, `last_name`, `dob`, `email`, `password`) VALUES ('$first_name', '$last_name', '$dob', '$email', '$hashed_pass')";
							$result= mysqli_query($con,$sql);
							if(!$result)
								{
								die("Could not create account");
								}
							else
								{
									//Account created, redirect to login page
									echo "<script type='text/javascript'>window.location= 'login.html';</script>";
									echo "<script type='text/javascript'>alert('Account created.');</script>";
								}
						}
						elseif($acc_type==2)	//Create teacher account
						{
							$sql = "INSERT INTO `teachers` (`first_name`, `last_name`, `dob`, `email`, `password`) VALUES ('$first_name', '$last_name', '$dob', '$email', '$hashed_pass')";
							$result= mysqli_query($con,$sql);
							if(!$result)
								{
									die("Could not create account");
								}
							else
								{
									echo "<script type='text/javascript'>window.location= 'login.html';</script>";
									echo "<script type='text/javascript'>alert('Account created.');</script>";
								}
						}
						else
						{
							echo "<script type='text/javascript'>alert('Invalid account type!');</script>";
						}
					}
				}
	}
?>